#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    request = require('request'),
    manifest = require('../CloudronManifest.json'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var TEST_TIMEOUT = 20000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var LOCATION = 'test';
    var LOCAL_FILENAME = 'sticker.png';
    var REMOTE_FILENAME = 'sticker.png';
    var CONTACT_NAME = 'Johannes';
    var CONTACT_EMAIL = 'foo@bar.com';

    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var adminUser = 'admin';
    var adminPassword = 'changeme';

    var app;
    var browser;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    function testFileDownload(callback) {
        var data = {
            url: `https://${app.fqdn}/remote.php/webdav/${REMOTE_FILENAME}`,
            auth: { username: username, password: password },
            encoding: 'binary'
        };

        request.get(data, function (error, response, body) {
            if (error !== null) return callback(error);
            if (response.statusCode !== 200) return callback('Status code: ' + response.statusCode);
            if (body !== fs.readFileSync(path.resolve(LOCAL_FILENAME)).toString('binary')) return callback('File corrupt');

            callback(null);
        });
    }

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(username, password, callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return browser.wait(until.elementLocated(By.id('user')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(By.id('user'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(By.id('user')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.id('password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//*[@id="expand"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//*[@id="expand"]'))), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get(`https://${app.fqdn}`);

        waitForElement(By.xpath('//*[@id="expand"]')).then(function () {
            return browser.findElement(By.xpath('//*[@id="expand"]')).click();
        }).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return waitForElement(By.xpath('//a[contains(@href, "logout")]'));
        }).then(function () {
            return browser.findElement(By.xpath('//a[contains(@href, "logout")]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//input[@name="user"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function closeWizard(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.id('firstrunwizard'));
        }).then(function () {
            return waitForElement(By.xpath('//div[@id="firstrunwizard"]//button[contains(@class, "icon-close")]'));
        }).then(function () {
            var button = browser.findElement(By.xpath('//div[@id="firstrunwizard"]//button[contains(@class, "icon-close")]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@id="firstrunwizard"]//button[contains(@class, "icon-close")]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//h2[contains(text(), "Good ")]')); // can be afternoon or evening!
        }).then(function () {
            // give it some time to save
            return browser.sleep(5000);
        }).then(function () {
            callback();
        });
    }

    function listUsers(callback) {
        browser.get(`https://${app.fqdn}/settings/users`).then(function () {
            // should see admin user
            return waitForElement(By.xpath('//div[@class="user-list-grid"]//div[contains(text(), "admin")]'));
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath(`//div[@class="user-list-grid"]//div[contains(text(), "${username}")]`)), TEST_TIMEOUT);
        }).then(function () {
            callback();
        }); // should see external user
    }

    function getContact(callback) {
        browser.get(`https://${app.fqdn}/apps/contacts`).then(function () {
            return browser.wait(until.elementLocated(By.xpath(`//div[contains(text(), "${CONTACT_NAME}")]`)), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function checkSetupWarnings(callback) {
        browser.get(`https://${app.fqdn}/settings/admin/overview`).then(function () {
            return waitForElement(By.xpath(`//span[contains(text(), "All checks passed.")]`));
        }).then(function () {
            callback();
        });
    }

    function uploadFile(done) {
        // was unable to upload the file correctly using node, too much time wasted...
        var cmd = `curl --insecure --http1.1 -X PUT -u ${username}:${password} "https://${app.fqdn}/remote.php/webdav/${REMOTE_FILENAME}" --data-binary @"./${LOCAL_FILENAME}"`;
        execSync(cmd);
        done();
    }

    function enableContacts(done) {
        browser.get(`https://${app.fqdn}/settings/apps/office/contacts`).then(function () {
            // enable contacts
            return waitForElement(By.xpath('//section[@id="tab-desc"]//input[@value="Download and enable"]'));
        }).then(function () {
            return browser.findElement(By.xpath('//section[@id="tab-desc"]//input[@value="Download and enable"]')).click();
        }).then(function () {
            // give it some time to process
            return browser.sleep(4000);
        }).then(function () {
            // wait for the app being present in the grid
            return waitForElement(By.xpath('//a[@href="/apps/contacts/"]'));
        }).then(function () {
            done();
        });
    }

    function addContact(done) {
        browser.get(`https://${app.fqdn}/apps/contacts`);

        waitForElement(By.id('new-contact-button')).then(function () {
            // click new contact
            return browser.findElement(By.id('new-contact-button')).click();
        }).then(function () {
            // add new contact
            return waitForElement(By.id('contact-fullname'));
        }).then(function () {
            return browser.findElement(By.id('contact-fullname')).clear();
        }).then(function () {
            return browser.findElement(By.id('contact-fullname')).sendKeys(CONTACT_NAME);
        }).then(function () {
            return browser.findElement(By.xpath('//*[@inputmode="email"]')).click();
        }).then(function () {
            return browser.findElement(By.xpath('//*[@inputmode="email"]')).sendKeys(CONTACT_EMAIL);
        }).then(function () {
            // give it some time to save
            return browser.sleep(5000);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('check scheduler script', function () {
        var output = execSync(`cloudron exec --app ${app.id} -- ls -l ${manifest.addons.scheduler.housekeeping.command}`);
        expect(output.indexOf('-rwxr-xr-x')).to.be.greaterThan(-1);
    });

    it('can login', login.bind(null, username, password));
    it('can close the wizard', closeWizard);
    it('can logout', logout);
    it('can upload file', uploadFile);
    it('can download previously uploaded file', testFileDownload);
    it('can login as admin', login.bind(null, adminUser, adminPassword));
    it('can close wizard', closeWizard);
    it('can list users', listUsers);
    it('can enable contacts app', enableContacts);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can get contact', getContact);
    it('can logout', logout);
    it('can download previously uploaded file', testFileDownload);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can get contact', getContact);
    it('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);
    it('can download previously uploaded file', testFileDownload);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);

            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');

            done();
        });
    });

    it('can login', login.bind(null, adminUser, adminPassword));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can get contact', getContact);
    it('has no setup warnings', checkSetupWarnings);
    it('can download previously uploaded file', testFileDownload);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.nextcloud.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can upload file', uploadFile);
    it('can download previously uploaded file', testFileDownload);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can close the wizard', closeWizard);
    it('can enable contacts app', enableContacts);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can get contact', getContact);
    it('has no setup warnings', checkSetupWarnings);
    it('can logout', logout);
    it('can download previously uploaded file', testFileDownload);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
