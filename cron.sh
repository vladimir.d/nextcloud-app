#!/bin/bash

echo "=> Run cron job"

exec /usr/local/bin/gosu www-data:www-data php -f /app/code/cron.php
