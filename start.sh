#!/bin/bash

set -eu

export mail_from_sub=$(echo $CLOUDRON_MAIL_FROM | cut -d \@ -f 1)
export mail_domain_sub=$(echo $CLOUDRON_MAIL_FROM | cut -d \@ -f 2)

mkdir -p /run/nextcloud/sessions

# ensure permissions for setup
chown -R www-data.www-data /app/data /run/nextcloud

if [[ -z "$(ls -A /app/data)" ]]; then
    echo "=> Detected first run"
    sudo -u www-data bash -c 'mkdir -p /app/data/config'
    sudo -u www-data bash -c 'cp -rf /app/pkg/apps_template /app/data/apps'

    # note: the install command patches this htaccess (this is for the code). it also generates /app/data/.htaccess for data-dir
    sudo -u www-data cp /app/pkg/htaccess.template /app/data/htaccess

    echo "=> Install nextcloud"
    # https://docs.nextcloud.com/server/14/admin_manual/configuration_server/occ_command.html#command-line-installation-label
    sudo -u www-data php /app/code/occ  maintenance:install --database "pgsql" --database-name "${CLOUDRON_POSTGRESQL_DATABASE}"  --database-user "${CLOUDRON_POSTGRESQL_USERNAME}" --database-pass "${CLOUDRON_POSTGRESQL_PASSWORD}" --database-host "${CLOUDRON_POSTGRESQL_HOST}" --database-port ${CLOUDRON_POSTGRESQL_PORT} --admin-user "admin" --admin-pass "changeme" --data-dir "/app/data" -n
else
    NEW_APPS="/app/pkg/apps_template"
    OLD_APPS="/app/data/apps"

    echo "=> Updating apps"

    echo "Old apps:"
    ls "${NEW_APPS}/"
    ls "${OLD_APPS}/"

    for app in `find "${NEW_APPS}"/* -maxdepth 0 -type d -printf "%f\n"`; do
        echo "Update app: ${app}"
        rm -rf "${OLD_APPS}/${app}"
        cp -rf "${NEW_APPS}/${app}" "${OLD_APPS}"
    done

    echo "New apps:"
    ls "${NEW_APPS}/"
    ls "${OLD_APPS}/"

    tables=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -t -c "SELECT table_name FROM information_schema.tables WHERE table_schema='public';")

    for table in ${tables}; do
        if [[ $table != oc_* ]]; then
            echo "${table} -> oc_${table}"
            PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "ALTER TABLE ${table} RENAME TO oc_${table}"
        fi
    done

    # note: there is also an auto-generated /app/data/.htaccess created by the install script for the data directory
    # this one is for the code directory
    if [[ ! -f /app/data/htaccess ]]; then
        echo "Copying htaccess"
        sudo -u www-data cp /app/pkg/htaccess.template /app/data/htaccess
    fi
fi

# ensure symlink for scss files
rm -f /app/data/core && ln -s /app/code/core /app/data/core

# maybe we can use '/app/code/occ config:system:set htaccess.RewriteBase --value="/"'
# htaccess.RewriteBase has to be a path and non-empty string for nextcloud to generate redirect rules in htaccess
echo "=> update config"
sudo -u www-data --preserve-env php <<'EOF'
<?php
    require_once "/app/data/config/config.php";
    $db = parse_url(getenv('CLOUDRON_POSTGRESQL_URL'));
    $runtime_config = array (
        'trusted_domains' => array ( 0 => getenv('CLOUDRON_APP_DOMAIN') ),
        'trusted_proxies' => array ( 0 => '172.18.0.1' ),
        'forcessl' => getenv('CLOUDRON'), # if unset/false, nextcloud sends a HSTS=0 header
        'mail_smtpmode' => 'smtp',
        'mail_smtpauth' => 'login',
        'mail_smtphost' => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
        'mail_smtpport' => getenv('CLOUDRON_MAIL_SMTP_PORT'),
        'mail_smtpname' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
        'mail_smtppassword' => getenv('CLOUDRON_MAIL_SMTP_PASSWORD'),
        'mail_from_address' => getenv('mail_from_sub'),
        'mail_domain' => getenv('mail_domain_sub'),
        'overwrite.cli.url' => getenv('CLOUDRON_APP_ORIGIN').'/',
        'overwritehost' => getenv('CLOUDRON_APP_DOMAIN'),
        'overwriteprotocol' => 'https',
        'dbtype' => 'pgsql',
        'dbname' => substr($db['path'], 1),
        'dbuser' => $db['user'],
        'dbpassword' => $db['pass'],
        'dbhost' => $db['host'],
        'dbtableprefix' => 'oc_',
        'updatechecker' => false,
        'updater.release.channel' => 'cloudron',
        'logfile' => '/dev/stderr', # default log is in data directory
        'loglevel' => '3', # set to 0 for debugging
        'debug' => false,
        'redis' => array(
            'host' => getenv('CLOUDRON_REDIS_HOST'),
            'port' => getenv('CLOUDRON_REDIS_PORT'),
            'password' => getenv('CLOUDRON_REDIS_PASSWORD')
        ),
        'memcache.local' => '\OC\Memcache\Redis',
        'memcache.locking' => '\OC\Memcache\Redis',
        'integrity.check.disabled' => true,
        'htaccess.RewriteBase' => '/',
        'simpleSignUpLink.shown' => false
    );

    if (!$CONFIG['default_phone_region']) $CONFIG['default_phone_region'] = 'US';

    $CONFIG = array_replace($CONFIG, $runtime_config);
    file_put_contents("/app/data/config/config.php", "<?php\n\$CONFIG = " . var_export($CONFIG, true) . ";\n");
EOF

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

mkdir -p /app/data/apache
[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

# ensure permissions after setup
chown -R www-data.www-data /app/data /run

echo "=> run migration"
sudo -u www-data php /app/code/occ upgrade
# https://docs.nextcloud.com/server/15/admin_manual/configuration_database/bigint_identifiers.html#bigint-64bit-identifiers
sudo -u www-data php /app/code/occ db:convert-filecache-bigint --no-interaction
# the RewriteBase requires additional htaccess rules that this command will generate
sudo -u www-data php /app/code/occ maintenance:update:htaccess

# patch htaccess to contain full url (see #59). this code does not re-patch across restarts
sed -e 's,caldav /,caldav https:\/\/%{HTTP_HOST}/,' \
    -e 's,carddav /,carddav https:\/\/%{HTTP_HOST}/,' -i /app/data/htaccess

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "=> Setting up LDAP integration"

    # enable ldap
    sudo -u www-data php /app/code/occ app:enable user_ldap || true

    # configure ldap
    # the first argument is the first config id, which is an empty string!
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_host --value "ldap://${CLOUDRON_LDAP_SERVER}"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_port --value "${CLOUDRON_LDAP_PORT}"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_base --value "${CLOUDRON_LDAP_USERS_BASE_DN}"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_base_users --value "${CLOUDRON_LDAP_USERS_BASE_DN}"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_base_groups --value "${CLOUDRON_LDAP_GROUPS_BASE_DN}"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_email_attr --value "mail"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_loginfilter_email --value "1"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_loginfilter_username --value "1"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_userfilter_objectclass --value "user"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_configuration_active --value "1"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_display_name --value "displayname"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_userlist_filter --value "(|(objectclass=user))"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_login_filter --value "(&(objectclass=user)(|(username=%uid)(mail=%uid)))"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_attributes_for_user_search --value "$(echo -e displayName\\nmail)"
    sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_expert_username_attr --value "username"

    # https://docs.nextcloud.com/server/stable/admin_manual/configuration_user/user_auth_ldap_api.html (ldapExpertUUIDUserAttr)
    if [[ -f /app/data/.ldap_uid ]]; then
        sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_expert_uuid_user_attr --value "uid"
    else
        sudo -u www-data php /app/code/occ config:app:set user_ldap ldap_expert_uuid_user_attr --value "username"
    fi
fi

# pre-install nextcloud talk aka spreed. this also enables spreed automatically on first install
# if already installed, this command does nothing and does not re-enable spreed if the user deactivated it
sudo -u www-data php /app/code/occ app:install spreed || true
sudo -u www-data php /app/code/occ config:app:set spreed stun_servers --value "[\"${CLOUDRON_STUN_SERVER}:${CLOUDRON_STUN_PORT}\"]"
sudo -u www-data php /app/code/occ config:app:set spreed turn_servers --value "[{\"server\":\"${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}\",\"secret\":\"${CLOUDRON_TURN_SECRET}\",\"protocols\":\"udp,tcp\"}]"

# add indices of the share table (see #61)
sudo -u www-data php /app/code/occ db:add-missing-indices
sudo -u www-data php /app/code/occ db:add-missing-columns

# this sometimes fails due to existing duplicate fileids in the oc_filecache_extended table so we have to apply that workaround
if ! sudo -u www-data php /app/code/occ db:add-missing-primary-keys; then
    echo "=> Applying oc_filecache_extended workaround"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "CREATE TABLE foobar AS SELECT fileid, MAX(metadata_etag) AS metadata_etag, MAX(creation_time) AS creation_time, MAX(upload_time) AS upload_time FROM oc_filecache_extended GROUP BY fileid;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "ALTER TABLE oc_filecache_extended RENAME TO save_oc_filecache_extended;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "ALTER TABLE foobar RENAME TO oc_filecache_extended;"
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "DROP TABLE save_oc_filecache_extended;"

    sudo -u www-data php /app/code/occ db:add-missing-primary-keys
fi

# now disable maintenance mode in case it was set
sudo -u www-data php /app/code/occ maintenance:mode --off

# start
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
